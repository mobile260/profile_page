import 'package:flutter/material.dart';

enum APP_THEME {LIGHT , DARK }
  void main() {
  runApp(ContactProfilePage());
  }
  class MyAppTheme {
  static ThemeData appThemeLight() {
  return ThemeData(
  brightness: Brightness.light,
  appBarTheme: AppBarTheme(
  color: Colors.white,
  iconTheme: IconThemeData(
  color: Colors.black
  )
  ),
  iconTheme: IconThemeData(
  color: Colors.indigo.shade500
  )
  );
  }
  static ThemeData appThemeDark() {
  return ThemeData(
  brightness: Brightness.dark
  );
  }}
  class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
  }
  class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context){
  return MaterialApp(
  debugShowCheckedModeBanner: false,
  theme: currentTheme == APP_THEME.DARK
  ? MyAppTheme.appThemeLight()
      : MyAppTheme.appThemeDark(),
  home: Scaffold(
  appBar: buildAppBarWidget(),
  body: buildBodyWidget(),
  floatingActionButton: FloatingActionButton(
  child: Icon(Icons.threesixty),
  onPressed: (){
  setState(() {
  currentTheme == APP_THEME.DARK
  ? currentTheme = APP_THEME.LIGHT
      : currentTheme = APP_THEME.DARK;
  });
  },

  ),
  ),
  );


  }
  }
  Widget profileActionItem(){
  return Row(
  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  children: <Widget>[
  buildCallButton(),
  buildTextButton(),
  buildVideoCallButton(),
  buildEmailButton(),
  buildDiretionsButton(),
  buildPayButton(),
  ],
  );
  }
  Widget buildCallButton() {
  return Column(
  children: <Widget>[
  IconButton(
  icon: Icon(
  Icons.call,
  color: Colors.indigo,
  ),
  onPressed: () {},
  ),
  Text("Call"),
  ],
  );
  }
  Widget buildTextButton() {
  return Column(
  children: <Widget>[
  IconButton(
  icon: Icon(
  Icons.message,
  color: Colors.indigo,
  ),
  onPressed: () {},
  ),
  Text("Text"),
  ],
  );
  }
  Widget buildVideoCallButton() {
  return Column(
  children: <Widget>[
  IconButton(
  icon: Icon(
  Icons.video_call,
  color: Colors.indigo,
  ),
  onPressed: () {},
  ),
  Text("Video"),
  ],
  );
  }
  Widget buildEmailButton() {
  return Column(
  children: <Widget>[
  IconButton(
  icon: Icon(
  Icons.email,
  color: Colors.indigo,
  ),
  onPressed: () {},
  ),
  Text("Email"),
  ],
  );
  }
  Widget buildDiretionsButton() {
  return Column(
  children: <Widget>[
  IconButton(
  icon: Icon(
  Icons.directions,
  color: Colors.indigo,
  ),
  onPressed: () {},
  ),
  Text("Directions"),
  ],
  );
  }
  Widget buildPayButton() {
  return Column(
  children: <Widget>[
  IconButton(
  icon: Icon(
  Icons.attach_money_outlined,
  color: Colors.indigo,
  ),
  onPressed: () {},
  ),
  Text("Pay"),
  ],
  );
  }
  Widget mobilePhoneListTile(){
  return ListTile(
  leading: Icon(Icons.call),
  title: Text('092-5556529'),
  subtitle: Text('mobile'),
  trailing:IconButton(
  icon: Icon(Icons.message),
  color: Colors.teal,
  onPressed: (){},
  ),
  );

  }
  Widget otherPhoneListTile(){
  return ListTile(
  leading: Text(""),
  title: Text('440-440-3390'),
  subtitle: Text('other'),
  trailing:IconButton(
  icon: Icon(Icons.message),
  color: Colors.teal,
  onPressed: (){},
  ),
  );

  }
  Widget emailListTile(){
  return ListTile(
  leading: Icon(Icons.email),
  title: Text('63160212@go.buu.ac.th'),
  subtitle: Text('work'),
  trailing: Text("")

  );

  }
  Widget addressListTile(){
  return ListTile(
  leading: Icon(Icons.location_on),
  title: Text('Bang saen'),
  subtitle: Text('home'),
  trailing: IconButton(
  icon: Icon(Icons.assistant_direction),
  color: Colors.teal,
  onPressed: (){},
  ),

  );

  }
  AppBar buildAppBarWidget() {
  return AppBar(
  backgroundColor: Colors.teal,

  leading: Icon(
  Icons.arrow_back,
  color: Colors.white,
  ),
  actions: <Widget>[
  IconButton(
  onPressed: (){},
  icon: Icon(Icons.star_border),
  color: Colors.white
  )
  ],
  );
  }
  Widget buildBodyWidget(){
  return ListView(

  children: <Widget>[
  Column(
  children: <Widget>[
  Container(
  width: double.infinity,

  //Height constraint at Container widget level
  height: 250,

  child:

  Image.network(
  "https://scontent.fbkk10-1.fna.fbcdn.net/v/t39.30808-6/240586810_6713180958722575_5076591976951534476_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=174925&_nc_eui2=AeFhwg6jlC48KE5Vu55qNPPyNOZKN9fshnk05ko31-yGeRPPwGZ6Nl4wNhJKCGNCt2-obWOjxqTOeVr98VSJaBeb&_nc_ohc=0LG6Vv7OOqkAX9uQoWb&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfAIam3uFkzsHqOIdxFgdhnsbrqo10Wq9lME9Tx89lHNbA&oe=63A681CD",
  fit: BoxFit.cover,
  ),
  ),
  Container(
  height: 60,
  child: Row(
  mainAxisAlignment: MainAxisAlignment.start,
  children: <Widget>[
  Padding(
  padding: EdgeInsets.all(8.0),
  child: Text(" Pattraporn Noothong",
  style : TextStyle(fontSize: 30),
  ),
  ),

  ],
  ),
  ),
  Divider(
  color: Colors.grey,
  ),

  Container(
  margin: EdgeInsets.only(top: 8,bottom: 8),
  child: Theme(
  data: ThemeData(
  iconTheme: IconThemeData(
  color: Colors.teal,
  ),
  ),
  child : profileActionItem(),
  ),


  ),


  Divider(
  color: Colors.grey,
  ),
  mobilePhoneListTile(),
  otherPhoneListTile(),
  Divider(
  color: Colors.grey,

  ),
  emailListTile(),
  addressListTile()
  ],
  )
  ],
  );
  }



